TEMPLATE = app

QT += qml quick widgets charts testlib

SOURCES += main.cpp \
    radardata.cpp \
    randomdata.cpp \
    plotsettings.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES +=

HEADERS += \
    radardata.h \
    randomdata.h \
    plotsettings.h

win32: LIBS += -L$$PWD/../../../../../../myLibs/radarSocket/lib/ -lRadarSocket

INCLUDEPATH += $$PWD/../../../../../../myLibs/radarSocket/include
DEPENDPATH += $$PWD/../../../../../../myLibs/radarSocket/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../../../../myLibs/radarSocket/lib/RadarSocket.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../../../../myLibs/radarSocket/lib/libRadarSocket.a
