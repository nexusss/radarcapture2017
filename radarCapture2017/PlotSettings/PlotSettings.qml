import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.1

Window {
    id: root
    width: 300
    height: 280

    property string backColor: ""
    property string sweep1Color: ""
    property string sweep2Color: ""
    property string borderColor: ""
    property int sweepWidth: 2

    ColumnLayout{
        anchors.fill: parent
        ColorSetings{
            labelText: "background"
            colorChoose: backColor
            width: root.width
            onColorChooseChanged: backColor = colorChoose
        }
        ColorSetings{
            labelText: "sweep 1"
            colorChoose: sweep1Color
            width: root.width
            onColorChooseChanged: sweep1Color = colorChoose
        }
        ColorSetings{
            labelText: "sweep 2"
            colorChoose: sweep2Color
            width: root.width
            onColorChooseChanged: sweep2Color = colorChoose
        }
        ColorSetings{
            labelText: "border"
            colorChoose: borderColor
            width: root.width
            onColorChooseChanged: borderColor = colorChoose
        }

        RowLayout{
            Label{
                text: "border width"
                font.pointSize: 16
                Layout.minimumWidth: 150
            }

            SpinBox {
                from: 1
                to: 99
                value: sweepWidth < 1 ? 1 : sweepWidth
                onValueChanged: sweepWidth = value
            }
        }
    }

}
