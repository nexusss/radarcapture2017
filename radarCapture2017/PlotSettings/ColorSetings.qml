import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0

Item{

    height: 40

    property string labelText: ""
    property string colorChoose: ""
    property int textWidth: 150

    ColorDialog {
      id: backGrcolorDialog
      title: "Please choose a color"
      color: colorChoose
      onAccepted: colorChoose = color
    }
    Label {
        id: lab
        x: 0
        y: 0
        clip: true
        height: parent.height
        width: textWidth
        text: labelText
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.pointSize: 16
    }
    Rectangle{
        id: backGroundColor
        height: parent.height
        color: colorChoose
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: lab.width
        MouseArea{
            anchors.fill: parent
            onClicked: {
                backGrcolorDialog.open()
                backGrcolorDialog.setColor(colorChoose)
            }
        }
    }
}
