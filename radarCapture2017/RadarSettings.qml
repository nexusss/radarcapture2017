import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0

Window {
    id: root
    width: 200
    height: 280
    signal applyClicked()
    signal cancelClicked()

    Text {
        id: harmWeightText
        x: 23
        y: 44
        width: 122
        height: 20
        text: "Harm weight"
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16
    }

    TextEdit {
        id: udpPortEdit
        x: 151
        y: 44
        width: 41
        height: 20
        text: qsTr("0.5")
        textFormat: Text.PlainText
        font.pixelSize: 18
    }

    Text {
        id: udpPortText2
        x: 22
        y: 17
        width: 122
        height: 20
        text: qsTr("UDP Port")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16
    }

    TextEdit {
        id: udpPortEdit2
        x: 150
        y: 17
        width: 42
        height: 20
        text: qsTr("63")
        textFormat: Text.PlainText
        font.pixelSize: 18
    }

    Text {
        id: udpPortText3
        x: 22
        y: 70
        width: 122
        height: 20
        text: qsTr("Number of packet")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16
    }

    TextEdit {
        id: udpPortEdit3
        x: 150
        y: 70
        width: 42
        height: 20
        text: qsTr("16")
        textFormat: Text.PlainText
        font.pixelSize: 18
    }

    Switch {
        id: sweepSwtch
        x: 23
        y: 96
        width: 169
        height: 40
        text: "2 Sweeps"
    }

    CheckBox {
        id: rawDataCheckBox
        x: 22
        y: 142
        text: "Enable Raw Data"
    }

    CheckBox {
        id: fftCheckBox
        x: 23
        y: 188
        text: "Use FFT"
    }

    Row {
        id: row1
        x: 4
        y: 234
        width: 192
        height: 39
        spacing: 13

        Button {
            id: applyButton
            width: 90
            text: "Apply"
            onClicked: applyClicked()
        }

        Button {
            id: cancelButton
            width: 90
            text: "Cancel"
            onClicked: cancelClicked()
        }
    }

}
