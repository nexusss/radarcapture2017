#ifndef RANDOMDATA_H
#define RANDOMDATA_H

#include <QObject>
#include <radarsocket.h>
#include <QTimer>

class RandomData : public QObject
{
    Q_OBJECT
    QTimer *timer;
public:
    explicit RandomData(QObject *parent = 0);
    void init();
signals:
    void sendData(Sweep peek,const int sweepNumber);

};

#endif // RANDOMDATA_H
