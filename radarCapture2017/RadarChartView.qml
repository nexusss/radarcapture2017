import QtQuick 2.0
import QtCharts 2.1

ChartView {
    id: chartView
    property real minAxisY1: 0
    property real maxAxisY1: 1000
    property real minAxisX: 0
    property real maxAxisX: 1000
    property real speedK: 100
    property string lineSeries1Color: ""
    property string lineSeries2Color: ""
    property string lineBorderColor: ""
    property int lineSeriesWidth: 2

    property real borderLeftValue: 0
    property real borderRightValue: 512
    property real borderBottomValue: 100

legend.borderColor: "#2abef4"
    legend.visible: false
    ValueAxis {
        id: axisY1
        min: minAxisY1
        max: maxAxisY1
        onMinChanged: {
            if(rightBorder.count > 0 && leftBorder.count > 0){
                rightBorder.replace(0,borderRightValue,min)
                leftBorder.replace(0,borderLeftValue,min)
            }
        }
        onMaxChanged: {
            if(rightBorder.count > 1 && leftBorder.count > 1){
                rightBorder.replace(1,borderRightValue,max)
                leftBorder.replace(1,borderLeftValue,max)
            }
        }
    }


    ValueAxis {
        id: axisX1
        min: minAxisX
        max: maxAxisX
        onMinChanged: {
            if(bottomBorder.count > 0)
                bottomBorder.replace(0,min,borderBottomValue)
        }
        onMaxChanged: {
            if(bottomBorder.count > 0)
                bottomBorder.replace(1,max,borderBottomValue)
        }
    }

    LineSeries{
        id: rightBorder
        axisX: axisX1
        axisY: axisY1
        color: lineBorderColor
        width: lineSeriesWidth
        useOpenGL: true
        XYPoint{x: borderRightValue; y: axisY1.min}
        XYPoint{x: borderRightValue; y: axisY1.max}
        onColorChanged: console.log("color bor",color)
    }

    LineSeries{
        id: leftBorder
        axisX: axisX1
        axisY: axisY1
        color: lineBorderColor
        width: lineSeriesWidth
        useOpenGL: true
        XYPoint{x: borderLeftValue; y: axisY1.min}
        XYPoint{x: borderLeftValue; y: axisY1.max}
    }

    LineSeries{
        id: bottomBorder
        axisX: axisX1
        axisY: axisY1
        color: lineBorderColor
        width: lineSeriesWidth
        useOpenGL: true
        XYPoint{x: axisX1.min; y: borderBottomValue}
        XYPoint{x: axisX1.max; y: borderBottomValue}
    }

    LineSeries {
        id: lineSeries1
        name: "sweep 1"
        axisX: axisX1
        axisY: axisY1
        color: lineSeries1Color
        width: lineSeriesWidth
        useOpenGL: true

    }
    LineSeries {
        id: lineSeries2
        name: "sweep 2"
        axisX: axisX1
        axisY: axisY1
        color: lineSeries2Color
        width: lineSeriesWidth
        useOpenGL: true
    }



    MouseArea {
        id: mouseArea1
        anchors.fill: parent
        property int startMouseX: 0
        property int startMouseY: 0
        onPressed: {
            startMouseX = mouseX
            startMouseY = mouseY
        }
        onReleased: {
            startMouseX = 0
            startMouseY = 0
        }

        onMouseXChanged: {
            var pixels = mouseX - startMouseX
            startMouseX = mouseX
            chartView.scrollLeft(pixels)
            maxAxisX = axisX1.max
            minAxisX = axisX1.min
        }
        onMouseYChanged: {
            var pixels = mouseY - startMouseY
            startMouseY = mouseY
            chartView.scrollUp(pixels)
            maxAxisY1 = axisY1.max
            minAxisY1 = axisY1.min
        }
    }

    Timer {
        id: refreshTimer
        interval: 1 / 60 * 1000 // 60 Hz
        running: true
        repeat: true
        onTriggered: {
//            radarData.updateData(chartView.series(0),0);
//            radarData.updateData(chartView.series(1),1);
        }
    }

    function zoomY1(){
        var delta = axisY1.max - axisY1.min
        if (delta > 2){

            var deltaK = delta / speedK
            if (deltaK < 1)
                deltaK = 1
            minAxisY1 += deltaK
            maxAxisY1 -= deltaK
        }
    }

    function unzoomY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1
        minAxisY1 -= deltaK
        maxAxisY1 += deltaK
    }

    function zoomX(){
        var delta = axisX1.max - axisX1.min
        if (delta > 2){

            var deltaK = delta / speedK
            if (deltaK < 1)
                deltaK = 1
            minAxisX += deltaK
            maxAxisX -= deltaK
        }
    }

    function unzoomX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        minAxisX -= deltaK
        maxAxisX += deltaK
    }
    function upY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        minAxisY1 += deltaK
        maxAxisY1 += deltaK

    }

    function downY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        minAxisY1 -= deltaK
        maxAxisY1 -= deltaK
    }

    function rightX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        minAxisX -= deltaK
        maxAxisX -= deltaK

    }

    function leftX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

            minAxisX += deltaK
            maxAxisX += deltaK
        }
}
