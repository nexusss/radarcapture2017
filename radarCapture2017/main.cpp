#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "radardata.h"
#include <string>
#include "plotsettings.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<PlotSettings>("dokltd.plotSettings", 1, 0, "PlotSettings");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    RadarData *radarData = new RadarData(0);

    engine.rootContext()->setContextProperty("radarData",radarData);
    std::string str;
    return app.exec();
}
