#ifndef PLOTSETTINGS_H
#define PLOTSETTINGS_H

#include <QObject>
#include <QSettings>
#include <QTimer>
#include <QDebug>

class PlotSettings : public QObject
{
    Q_OBJECT

    QSettings *cnf;

    QTimer saveTimer;

    Q_PROPERTY(qreal minX1 READ minX1 WRITE setMinX1 NOTIFY minX1Changed)
    Q_PROPERTY(qreal maxX1 READ maxX1 WRITE setMaxX1 NOTIFY maxX1Changed)
    Q_PROPERTY(qreal minY1 READ minY1 WRITE setMinY1 NOTIFY minY1Changed)
    Q_PROPERTY(qreal maxY1 READ maxY1 WRITE setMaxY1 NOTIFY maxY1Changed)

    Q_PROPERTY(QString backColor READ backColor WRITE setBackColor NOTIFY backColorChanged)
    Q_PROPERTY(QString sweep1Color READ sweep1Color WRITE setSweep1Color NOTIFY sweep1ColorChanged)
    Q_PROPERTY(QString sweep2Color READ sweep2Color WRITE setSweep2Color NOTIFY sweep2ColorChanged)
    Q_PROPERTY(QString borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)
    Q_PROPERTY(int sweepWidth READ sweepWidth WRITE setSweepWidth NOTIFY sweepWidthChanged)

    qreal m_minX1,m_maxX1,m_minY1,m_maxY1;

    QString m_backColor, m_sweep1Color, m_sweep2Color, m_borderColor;
    int m_sweepWidth;
public:
    explicit PlotSettings(QObject *parent = 0);
    ~PlotSettings();
    qreal minX1() const;
    void setMinX1(qreal minX1);
    qreal maxX1() const;
    void setMaxX1(qreal maxX1);
    qreal minY1() const;
    void setMinY1(qreal minY1);
    qreal maxY1() const;
    void setMaxY1(qreal maxY1);
    QString backColor() const;
    void setBackColor(QString backColor);
    QString sweep1Color() const;
    void setSweep1Color(QString sweep1Color);
    QString sweep2Color() const;
    void setSweep2Color(QString sweep2Color);
    QString borderColor() const;
    void setBorderColor(QString borderColor);
    int sweepWidth() const;
    void setSweepWidth(int sweepWidth);

    void saveSettings();
signals:
    void minX1Changed();
    void maxX1Changed();
    void minY1Changed();
    void maxY1Changed();
    void backColorChanged();
    void sweep1ColorChanged();
    void sweep2ColorChanged();
    void borderColorChanged();
    void sweepWidthChanged();
};

#endif // PLOTSETTINGS_H
