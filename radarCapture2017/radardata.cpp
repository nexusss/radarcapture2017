#include "radardata.h"
#include <QtCharts/QXYSeries>

RadarData::RadarData(QObject *parent) : QObject(parent)
{

    qRegisterMetaType<Sweep>("Sweep");
    radarSocket = new RadarSocket("radar.ini");
    radarSocket->moveToThread(&radarThread);
    connect(&radarThread,&QThread::started,radarSocket,&RadarSocket::init);
    connect(radarSocket,&RadarSocket::hasPacket,this,&RadarData::setData);
    connect(&radarThread,&QThread::finished,radarSocket,&RadarSocket::deleteLater);

    radarThread.start();

    randomData = new RandomData();
    randomData->moveToThread(&randomThread);
    connect(&randomThread,&QThread::started,randomData,&RandomData::init);
    connect(randomData,&RandomData::sendData,this,&RadarData::setData);
    connect(&randomThread,&QThread::finished,randomData,&RandomData::deleteLater);

    randomThread.start();


}

void RadarData::setData(Sweep peek,const int sweepNumber){
    if(sweepNumber < maxSweep){
        sweep[sweepNumber].clear();
        for( int i = 0; i < peek.size(); i++) {
            QPointF harm;
            harm.setX(i * harmWeight);
            harm.setY(0);

            QPointF harm2;
            harm2.setX(i * harmWeight);
            harm2.setY(peek.at(i ));

            QPointF harm3;
            harm3.setX(i * harmWeight);
            harm3.setY(0);


            sweep[sweepNumber].push_back(harm);
            sweep[sweepNumber].push_back(harm2);
            sweep[sweepNumber].push_back(harm3);
        }
    }
}

void RadarData::updateData(QAbstractSeries *series, const int sweepNumber){

    if (series && sweepNumber < maxSweep) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        // Use replace instead of clear + append, it's optimized for performance
        xySeries->replace(sweep[sweepNumber]);
    }

}
