import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import dokltd.plotSettings 1.0
Item {
    id: rPlot

    property string backColor: plotSettings.backColor
    property string sweep1Color: plotSettings.sweep1Color
    property string sweep2Color: plotSettings.sweep2Color
    property string borderColor: plotSettings.borderColor
    property int sweepWidth: plotSettings.sweepWidth

    onBackColorChanged: plotSettings.backColor = backColor
    onSweep1ColorChanged: plotSettings.sweep1Color = sweep1Color
    onSweep2ColorChanged: plotSettings.sweep2Color = sweep2Color
    onBorderColorChanged: plotSettings.borderColor = borderColor
    onSweepWidthChanged: plotSettings.sweepWidth = sweepWidth

    PlotSettings{
        id: plotSettings

        function updateRChartMinX1(){
            rChart.minAxisX = minX1
        }
        function updateRChartMaxX1(){
            rChart.maxAxisX = maxX1
        }
        function updateRChartMinY1(){
            rChart.minAxisY1 = minY1
        }
        function updateRChartMaxY1(){
            rChart.maxAxisY1 = maxY1
        }

        Component.onCompleted: {
            rChart.minAxisX = minX1
            rChart.maxAxisX = maxX1
            rChart.minAxisY1 = minY1
            rChart.maxAxisY1 = maxY1


        }
    }

    GridLayout{
        anchors.fill: parent
        columns: 2
        rows: 2
        rowSpacing: 0
        ColumnLayout {
            id: leftColumnLayout
            Layout.fillHeight: false

            Layout.row: 0
            Layout.column: 0
            Layout.minimumWidth: 100
            Layout.maximumWidth: 100
            Layout.fillWidth: true

            Label {
                id: label1
                Layout.fillWidth: true
                Layout.maximumWidth: parent.width
                Layout.minimumWidth: parent.width

                text: "Max"
                font.pointSize: 11
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            TextField {
                id: maxAxisY1Field
                Layout.fillWidth: true
                Layout.maximumWidth: parent.width
                Layout.minimumWidth: parent.width
                validator: DoubleValidator {bottom: parseFloat(minAxisY1Field.text) + 1}
                text: plotSettings.maxY1.toFixed(1)
                font.bold: true
                font.pointSize: 10
                onEditingFinished: {

                    plotSettings.maxY1 = parseFloat(text)
                    plotSettings.updateRChartMaxY1()
                }
                background: Rectangle {
                          border.color: "#21be2b"
                          radius: 5
                      }
            }

            Label {
                id: label2
                Layout.fillWidth: true
                Layout.maximumWidth: parent.width
                Layout.minimumWidth: parent.width

                text: "Min"
                font.pointSize: 11
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                Material.theme: Material.Dark
                Material.accent: Material.Red
            }

            TextField {
                id: minAxisY1Field
                Layout.fillWidth: true
                Layout.maximumWidth: parent.width
                Layout.minimumWidth: parent.width

                text: plotSettings.minY1.toFixed(1)
                validator: DoubleValidator {top: parseFloat(maxAxisY1Field.text) - 1}
                font.bold: true
                font.pointSize: 10
                Material.theme: Material.Dark
                Material.accent: Material.Red
                onEditingFinished: {

                    plotSettings.minY1 = parseFloat(text)
                    plotSettings.updateRChartMinY1()
                }
                background: Rectangle {
                          border.color: "#21be2b"
                          radius: 5
                      }
            }



            RowLayout{
                Layout.fillHeight: false
                Layout.fillWidth: true
                Button {
                    id: zoomYButton

                    background: Rectangle {
                              implicitWidth: 40
                              implicitHeight: 40
                              opacity: enabled ? 1 : 0.3
                              color: zoomYButton.down ? "#353535" : "#828282"
                              radius: 5
                          }
                    Layout.fillWidth: true
                    Image {

                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "icons/Zoom_in.png"
                    }
                    autoRepeat: true
                    onClicked: rChart.zoomY1()
                }

                Button {
                    id: unZoomYButton
                    background: Rectangle {
                              implicitWidth: 40
                              implicitHeight: 40
                              opacity: enabled ? 1 : 0.3
                              color: unZoomYButton.down ? "#353535" : "#828282"
                              radius: 5
                          }
                    Layout.fillHeight: false
                    Layout.fillWidth: true

                    Image {

                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "icons/Zoom_out.png"
                    }
                    autoRepeat: true
                    onClicked: rChart.unzoomY1()
                }


            }

            RowLayout{
                Layout.fillWidth: true
                Button {
                    id: upButton
                    background: Rectangle {
                            implicitWidth: 40
                            implicitHeight: 40
                            opacity: enabled ? 1 : 0.3
                            color: upButton.down ? "#353535" : "#828282"
                            radius: 5
                        }
                    Layout.fillWidth: true
                    Image {
                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "icons/up.png"
                    }
                    autoRepeat: true
                    onClicked: rChart.upY1()
                }
                Button {
                    id: downButton
                    background: Rectangle {
                        implicitWidth: 40
                        implicitHeight: 40
                        opacity: enabled ? 1 : 0.3
                        color: downButton.down ? "#353535" : "#828282"
                        radius: 5
                    }
                    Layout.fillWidth: true
                    Image {

                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "icons/down.png"
                    }
                    autoRepeat: true
                    onClicked: rChart.downY1()
                }
            }


        }
        RadarChartView {
            id: rChart

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 0
            Layout.column: 1

            backgroundColor: backColor
            lineSeries1Color: sweep1Color
            lineSeries2Color: sweep2Color
            lineSeriesWidth: sweepWidth
            lineBorderColor: { console.log("border col2",borderColor); borderColor}

            onMaxAxisY1Changed:  plotSettings.maxY1 = maxAxisY1.toFixed(1)
            onMinAxisY1Changed:  plotSettings.minY1 = minAxisY1.toFixed(1)
            onMaxAxisXChanged:   plotSettings.maxX1 = maxAxisX.toFixed(1)
            onMinAxisXChanged: plotSettings.minX1 = minAxisX.toFixed(1)
            onLineBorderColorChanged:     console.log("border col",borderColor)

        }
        RowLayout {
            id: bottomLayout
            Layout.row: 1
            Layout.column: 1
            Layout.fillWidth: true
            ColumnLayout{
                spacing: 1
            Label {
                id: label4

                text: "Min"
                Layout.fillWidth: false
                font.pointSize: 11
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            TextField {
                id: minAxisXField
                width: 100
                validator: DoubleValidator {top: parseFloat(maxAxisXField.text) - 1}
                text: plotSettings.minX1.toFixed(1)
                font.pointSize: 10
                font.bold: true
                Layout.maximumWidth: 100
                Layout.minimumWidth: 100
                onEditingFinished: {

                    plotSettings.minX1 = parseFloat(text)
                    plotSettings.updateRChartMinX1()
                }
                background: Rectangle {
                          border.color: "#21be2b"
                          radius: 5
                      }
            }
            }

            ColumnLayout{
                spacing: 1
            Label {
                id: label3

                text: "Max"
                font.pointSize: 11
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            TextField {
                id: maxAxisXField
                Layout.maximumWidth: 100
                Layout.minimumWidth: 100

                validator: DoubleValidator {id: val;bottom: parseFloat(minAxisXField.text) + 1;}
                text: plotSettings.maxX1.toFixed(1)
                font.pointSize: 10
                font.bold: true
                onEditingFinished: {

                    plotSettings.maxX1 = parseFloat(text)
                    plotSettings.updateRChartMaxX1()
                }
                background: Rectangle {
                          border.color: "#21be2b"
                          radius: 5
                      }

            }
            }
            Button {
                id: unZoomXButton
                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: unZoomXButton.down ? "#353535" : "#828282"
                    radius: 5
                }
                Layout.fillWidth: false
                Image {

                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent
                    source: "icons/Zoom_out.png"
                }
                autoRepeat: true
                onClicked: rChart.unzoomX()               
            }

            Button {
                id: zoomXButton
                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: zoomXButton.down ? "#353535" : "#828282"
                    radius: 5
                }
                Layout.fillWidth: false
                Image {

                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent
                    source: "icons/Zoom_in.png"
                }
                highlighted: true
                autoRepeat: true
                onClicked: rChart.zoomX()
            }

            Button {
                id: leftButton
                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: leftButton.down ? "#353535" : "#828282"
                    radius: 5
                }
                Layout.fillWidth: false
                Image {

                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent
                    source: "icons/left.png"
                }
                autoRepeat: true
                onClicked: rChart.leftX()
            }

            Button {
                id: rightButton
                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: rightButton.down ? "#353535" : "#828282"
                    radius: 5
                }
                Layout.fillWidth: false
                Image {

                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent
                    source: "icons/right.png"
                }
                autoRepeat: true
                onClicked: rChart.rightX()

            }
        }
    }
    Shortcut{
        sequence: "Right"
        onActivated: rChart.rightX()
    }
    Shortcut{
        sequence: "Left"
        onActivated: rChart.leftX()
    }
    Shortcut{
        sequence: "Up"
        onActivated: rChart.upY1()
    }

    Shortcut{
        sequence: "Down"
        onActivated: rChart.downY1()
    }
    Shortcut{
        sequence: "Ctrl+Right"
        onActivated: rChart.zoomX()
    }
    Shortcut{
        sequence: "Ctrl+Left"
        onActivated: rChart.unzoomX()
    }
    Shortcut{
        sequence: "Ctrl+Up"
        onActivated: rChart.zoomY1()
    }
    Shortcut{
        sequence: "Ctrl+Down"
        onActivated: rChart.unzoomY1()
    }
}
