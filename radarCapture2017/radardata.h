#ifndef RADARDATA_H
#define RADARDATA_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
#include <radarsocket.h>
#include <QThread>

#include "randomdata.h"

QT_CHARTS_USE_NAMESPACE

class RadarData : public QObject
{
    Q_OBJECT
    RadarSocket *radarSocket;
    QThread radarThread;

    RandomData *randomData;
    QThread randomThread;

    static const int maxSweep = 2;
    float harmWeight = 1;
    TwoSweep sweep[maxSweep];
public:
    explicit RadarData(QObject *parent = 0);

signals:

public slots:
    void setData(Sweep peek,const int sweepNumber);
    void updateData(QAbstractSeries *series, const int sweepNumber);
};

#endif // RADARDATA_H
