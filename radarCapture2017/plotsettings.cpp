#include "plotsettings.h"
#include <QDir>

PlotSettings::PlotSettings(QObject *parent) : QObject(parent)
{
    saveTimer.setSingleShot(true);
    saveTimer.setInterval(1 * 1000);

    connect(&saveTimer,&QTimer::timeout,this,&PlotSettings::saveSettings);

    qreal d_minX1 = 0;
    qreal d_maxX1 = 1000;
    qreal d_minY1 = 0;
    qreal d_maxY1 = 1000;

    QString settingsPath = QDir::homePath() + "/radarCapture";

    QString d_backColor = "gainsboro", d_sweep1Color = "royalblue", d_sweep2Color = "orange", d_borderColor = "gray";
    int d_sweepWidth = 3;

    QDir settingsDir;
    settingsDir.mkdir(settingsPath);

    cnf = new QSettings(settingsPath + "/plot.ini", QSettings::IniFormat);

    if(QFileInfo::exists(settingsPath + "/plot.ini")){
        setMinX1(cnf->value("minX1",d_minX1).toFloat());
        setMaxX1(cnf->value("maxX1",d_maxX1).toFloat());
        setMinY1(cnf->value("minY1",d_minY1).toFloat());
        setMaxY1(cnf->value("maxY1",d_maxY1).toFloat());
        setBackColor(cnf->value("backColor",d_backColor).toString());
        setSweep1Color(cnf->value("sweep1Color",d_sweep1Color).toString());
        setSweep2Color(cnf->value("sweep2Color",d_sweep2Color).toString());
        setBorderColor(cnf->value("borderColor",d_borderColor).toString());
        setSweepWidth(cnf->value("sweepWidth",d_sweepWidth).toInt());
    }
    else{
        setMinX1(d_minX1);
        setMaxX1(d_maxX1);
        setMinY1(d_minY1);
        setMaxY1(d_maxY1);
        setBackColor(d_backColor);
        setSweep1Color(d_sweep1Color);
        setSweep2Color(d_sweep2Color);
        setBorderColor(d_borderColor);
        setSweepWidth(d_sweepWidth);
    }

    qDebug() << "sett" << m_minX1 << m_maxX1 << m_minY1 << m_maxY1;
    saveSettings();
}

qreal PlotSettings::minX1() const{
    return m_minX1;
}

void PlotSettings::setMinX1(qreal minX1){

    this->m_minX1 = minX1;
    emit minX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal PlotSettings::maxX1() const{
    return m_maxX1;
}

void PlotSettings::setMaxX1(qreal maxX1){

    this->m_maxX1 = maxX1;
    emit maxX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal PlotSettings::minY1() const{
    return m_minY1;
}

void PlotSettings::setMinY1(qreal minY1){

    this->m_minY1 = minY1;
    emit minY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal PlotSettings::maxY1() const{
    return m_maxY1;
}

void PlotSettings::setMaxY1(qreal maxY1){
    this->m_maxY1 = maxY1;
    emit maxY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

QString PlotSettings::backColor() const{
    return m_backColor;
}

void PlotSettings::setBackColor(QString backColor){
    qDebug() << "back color" << backColor;
    this->m_backColor = backColor;
    saveTimer.stop();
    saveTimer.start();
}

QString PlotSettings::sweep1Color() const{
    return m_sweep1Color;
}

void PlotSettings::setSweep1Color(QString sweep1Color){
    this->m_sweep1Color = sweep1Color;
    saveTimer.stop();
    saveTimer.start();
}

QString PlotSettings::sweep2Color() const{
    return m_sweep2Color;
}

void PlotSettings::setSweep2Color(QString sweep2Color){
    this->m_sweep2Color = sweep2Color;
    saveTimer.stop();
    saveTimer.start();
}

QString PlotSettings::borderColor() const{
    return m_borderColor;
}

void PlotSettings::setBorderColor(QString borderColor){
    m_borderColor = borderColor;
    saveTimer.stop();
    saveTimer.start();
}

int PlotSettings::sweepWidth() const{
    return m_sweepWidth;
}

void PlotSettings::setSweepWidth(int sweepWidth){
    m_sweepWidth = sweepWidth;
    saveTimer.stop();
    saveTimer.start();
}

void PlotSettings::saveSettings(){
    qDebug() << "save";
    cnf->setValue("minX1",m_minX1);
    cnf->setValue("maxX1",m_maxX1);
    cnf->setValue("minY1",m_minY1);
    cnf->setValue("maxY1",m_maxY1);
    cnf->setValue("backColor",m_backColor);
    cnf->setValue("sweep1Color",m_sweep1Color);
    cnf->setValue("sweep2Color",m_sweep2Color);
    cnf->setValue("borderColor",m_borderColor);
    cnf->setValue("sweepWidth",m_sweepWidth);
}

PlotSettings::~PlotSettings(){
    delete cnf;
}

