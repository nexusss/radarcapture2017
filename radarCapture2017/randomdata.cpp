#include "randomdata.h"

RandomData::RandomData(QObject *parent) : QObject(parent)
{

}

void RandomData::init(){
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    timer = new QTimer();
    timer->setInterval(10);
    connect(timer,&QTimer::timeout,[=] () {
        Sweep sw1,sw2;
        for(int i = 0; i < 1024 * 8; i++){
           sw1.push_back(qrand()% 65000);
           sw2.push_back(qrand()% 65000);
        }
        emit sendData(sw1,0);
        emit sendData(sw2,1);
    });
    timer->start();
}


