import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "PlotSettings"
ApplicationWindow {

    width: 900
    height: 480
    visible: true

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Settings")
            MenuItem {
                text: qsTr("&Radar settings ")
                onTriggered: rs.show();
                shortcut:  "Ctrl+R"

            }
            MenuItem {
                text: qsTr("&Plot settings")
                onTriggered: pl.show();
                shortcut:  "Ctrl+P"

            }
            MenuItem {
                text: qsTr("E&xit")
                onTriggered: Qt.quit();
            }
        }
    }
    PlotSettings{
        id: pl

        onBackColorChanged: rPlot.backColor = backColor
        onSweep1ColorChanged: rPlot.sweep1Color = sweep1Color
        onSweep2ColorChanged: rPlot.sweep2Color = sweep2Color
        onBorderColorChanged: rPlot.borderColor = borderColor
        onSweepWidthChanged: rPlot.sweepWidth = sweepWidth
    }

    RadarPlot{
        id: rPlot
        anchors.fill: parent

        Component.onCompleted: {
            pl.backColor = backColor
            pl.sweep1Color = sweep1Color
            pl.sweep2Color = sweep2Color
            pl.borderColor = borderColor
            pl.sweepWidth = sweepWidth
        }
    }

    RadarSettings{
        id: rs
        onApplyClicked: {
            close()
        }
        onCancelClicked: close()

    }

}
